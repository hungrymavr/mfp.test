#include <stdio.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <sys/time.h>
#include <sys/types.h>
#include <time.h>
#include <getopt.h>
#include <stdbool.h>
#include <fcntl.h>

typedef struct {
    bool graph;
    int block_size;
    int count;
    char *file_name;

} config_t;

typedef struct {
    size_t size;
    char *data;
} package_t;

static config_t config;


void fps_calculate(const struct timespec *start, const struct timespec *stop, int frame_num);
void init_connection(int argc, char **argv);

void makepkg(package_t *pkg, const char *data, int shift, size_t data_size);

void draw(int dev_desc);
void draw_text(int dev_desc);
void draw_picture(int dev_desc);

void help(void);

int main(int argc, char **argv) {

    init_connection(argc, argv);

    int ddev = open(config.file_name, O_RDWR | O_NOCTTY);
    if(ddev == -1) {
        perror("Fail during open device file. Are you root? \nError message");
        return 0;
    }

    draw(ddev);

//    int fdes = open("/dev/ttyACM0", O_RDWR | O_NOCTTY);

//    char msg[1030] = { 0x24, 0x02, 0x02, 0x04, 0x00, 0x00 };
//    memset(msg + 6, 0x00, sizeof(msg) - 6);

//    char recv[10];
//    char shift;
//    memset(recv, 0x00, sizeof(recv));

//    struct timespec start, stop;
//    clock_gettime(CLOCK_REALTIME, &start);

//    int i, n;
//    long double fps;
//    for(i = 0; i < 1000; i++) {
//        if(i % 100 == 0) {
//            clock_gettime(CLOCK_REALTIME, &stop);
//            if(stop.tv_sec == start.tv_sec)fps = 20.0 / ((stop.tv_nsec - start.tv_nsec) / 1000000000.0);
//            printf("FPS: %f\n", fps );
//            clock_gettime(CLOCK_REALTIME, &start);
//        }
//        shift = i % 8;
//        memset(msg + 6, (0xC0 >> shift), sizeof(msg) - 6);

//        write(fdes, msg, sizeof(msg));
//        n = 0;
//        while( n == 0 ) n = read(fdes, recv, 10);
//    }


//    clock_gettime(CLOCK_REALTIME, &stop);
//    printf("Time: %ld sec %ld nsec.\n", (long) (stop.tv_sec - start.tv_sec), stop.tv_nsec - start.tv_nsec);


//    printf("Receive %i: %x %x %x %x %x %x %x %x %x\n ", n, recv[0],recv[1],recv[2],recv[3],recv[4],recv[5], recv[6], recv[7],recv[8]);
//    close(fdes);

    return 0;
}

void init_connection(int argc, char **argv) {
    if(argc == 1) help();

// default config
    config.block_size = 0;
    config.count = 0; // infinity
    config.file_name = malloc(strlen("/dev/ttyACM0"));
    strcpy(config.file_name, "/dev/ttyACM0");
    config.graph = true;

    const char s_opt[] = "s:c:d:tg";
    const struct option l_opt[] = {
        {"block-size", required_argument, NULL, 's'},
        {"count", required_argument, NULL, 'c'},
        {"device", required_argument, NULL, 'd'},
        {"text", no_argument, NULL, 't'},
        {"graph", no_argument, NULL, 'g'},
        {"help", no_argument, NULL, 'h'}
    };

    int result, opt_index = 0;
    while((result = getopt_long(argc, argv, s_opt, l_opt, opt_index)) != -1) {
        switch (result) {
        case 'h':   help(); break;

        case 's':   config.block_size = atoi(optarg); break;

        case 'c':   config.count = atoi(optarg); break;

        case 'd':
//            printf("File device : %s\n", optarg);
            free(config.file_name);
            config.file_name = malloc(sizeof(optarg));
            strcpy(config.file_name, optarg);
            break;

        case 't':   config.graph = false; break;

        case 'g':   config.graph = true; break;

        default:
            help();
            break;
        }
    }

    printf("\nTest will be starting with next configuration:\n");
    printf("Mode : %s\n", config.graph ? "graphical" : "text");
    if(config.block_size == 0) config.graph ? (config.block_size = 1024) : (config.block_size = 128);
    printf("Block size : %d\n", config.block_size);
    printf("Transaction count: ");
    config.count == 0 ? printf("infinity\n") : printf("%d\n", config.count);
    printf("Device file : %s\n\n", config.file_name);
}

void makepkg(package_t *pkg, const char *data, int shift, size_t data_size) {

    int shift_size;
    config.graph ? (shift_size = 2) : (shift_size = 1);
    pkg->size = data_size + shift_size + 4;

//    printf("Package size = %d\n", pkg->size);

    pkg->data = malloc(pkg->size);

    pkg->data[0] = 0x24; // start symbol
    pkg->data[1] = config.graph ? 0x02 : 0x01; // mode
    pkg->data[2] = (char) ((data_size + shift_size) );
    pkg->data[3] = (char) ((data_size + shift_size) >> 8);

    if(config.graph) {
        pkg->data[4] = (char) (shift);
        pkg->data[5] = (char) (shift >> 8);
    } else
        pkg->data[4] = (char) shift;

    memcpy(config.graph ? pkg->data + 6 : pkg->data + 5, data, data_size);
}

inline void draw(int dev_desc) {
    if(config.graph) {
        printf("Pic: \n");
        draw_picture(dev_desc);
    }
    else draw_text(dev_desc);
}

void draw_text(int dev_desc) {
    int i;
    char data[256];
    char receive[10];
    for(i = 0; i < 256; i++) data[i] = i;

    package_t pkg;


    for(i = 0; i < (config.count ? config.count : 70000); i++) {
        makepkg(&pkg, data + (i * config.block_size & 255), (i * config.block_size) & 127, config.block_size);
        write(dev_desc, pkg.data, pkg.size);
        free(pkg.data);
        while(!read(dev_desc, receive, 10));
    }


    printf("\n");
}

void draw_picture(int dev_desc) {
    char receive[10];
    package_t pkg;
    char data[7] = {0x24, 0x02, 0x03, 0x00, 0x00, 0x00, 0xFF};

    int i;

//    for(i = 0; i < (config.count ? config.count : 70000); i++) {

//        makepkg(&pkg, gimp_image.pixel_data, 0, 1024);
    for(i = 0; i < 256; i++) {
        data[4] = i;
//        makepkg(&pkg, data, 0, 1024);
        write(dev_desc, data, 7);
        free(pkg.data);
        while(!read(dev_desc, receive, 10));
    }

}

void help() {
    printf("Have a nice day ;)\n\n");
    exit(0);
}
